JEOS_CORE
=========

JEOS core layer builder

Requirements
------------

ansible latest version.

Role Variables
--------------

:D


Sample
------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: thapakazi.jeos_core }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
